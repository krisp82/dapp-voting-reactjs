import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import VotingChart from './voting_chart';

class App extends Component {
// Add Comment
// Add another push
// Add By Master1
// Add by Master2
// Add by Master3
  constructor(props)
  {
    super(props);
    this.state = {
      candidates: [{
        name: 'Wish',
        score: 0,
      },
      {
        name: 'Chaow',
        score: 0,
      },
      {
        name: 'Alex',
        score: 0,
      },
      {
        name: 'View',
        score: 0,
      },
      {
        name: 'Flash',
        score: 0,
      },
      {
        name: 'Nueng',
        score: 0,
      },
      {
        name: 'Simon',
        score: 0,
      },
      {
        name: 'Mateuos',
        score: 0,
      }]
    }
    // this.totalVotesFor = this.totalVotesFor.bind(this);
    this.voteForCandidate = this.voteForCandidate.bind(this);
  }

  // totalVotesFor(name)
  // {
  //    fetch('http://localhost:3001/candidate/'+name).
  //   then( (response) => response.json())
  //   .then( data => {
  //        console.log("ShowDAta: "+ JSON.stringify(data));
  //        this.setState({name, score: data});
  //      })
  //   .catch( (err) => {
  //     console.log(err);
  //     return -1;
  //   } );
  //
  // }

  voteForCandidate(name)
  {
     fetch('http://localhost:3001/candidate/vote/'+name).
    then( (response) => response.json()).
    then( (data) => {
      let candidates = this.state.candidates;
      let index = 0;
      candidates.map((k,i) => {
        if (k.name == name)
        {
          index = i;
          return ;
        }
      });
      candidates[index].score = data;
      this.setState({candidates}); })
    .catch( (err) => {
      console.log(err);
      return -1;
    });

  }

  render() {
    const {candidates} = this.state;
    const renderCandidates = candidates.map((k,i) => (
        <tr key={i}>
          <td>{k.name}</td>
          <td>{k.score}</td>
          <td><button onClick={() => this.voteForCandidate(k.name)}>Vote here</button></td>
        </tr>
    ));
    return (
      <div className="App">
        <header className="App-header">
          <table border="1">
            <thead>
              <tr>
                <th>Candidate Name</th>
                <th>Score</th>
                <th>Vote</th>
              </tr>
            </thead>
            <tbody>
              {renderCandidates}
            </tbody>
          </table>
          <VotingChart data={candidates} />
        </header>
      </div>
    );
  }
}

export default App;
