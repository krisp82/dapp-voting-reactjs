import React, { Component } from 'react';
import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';


class VotingChart extends Component {

  constructor(props)
  {
    super(props);
  }

  render() {
      let candidates = this.props.data;
      let data = candidates.map( (candidate) => ({name: candidate.name, uv: candidate.score }));
  return (
    <LineChart
      width={800}
      height={400}
      data={data}
      margin={{
        top: 5, right: 30, left: 20, bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      <Line type="monotone" dataKey="uv" name="score" stroke="#82ca9d" />
    </LineChart>
  );
  }
}

export default VotingChart;
